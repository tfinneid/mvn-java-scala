package no.fcon

import org.specs._

object BasicTest extends Specification {

  "IntegerOp" should {

    "add 2 numbers" in {
      1 + 1 mustEqual 3
    }
  }
}