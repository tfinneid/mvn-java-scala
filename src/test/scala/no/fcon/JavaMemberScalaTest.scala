package no.fcon

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object JavaMemberScalaTest extends Properties("IntegerOp"){

  property("add") = forAll { (a: Int, b: Int) =>
      a + b == a + b
  }

  property("addIntegerOp") = forAll { (a: Int, b: Int) =>
    IntegerOp.addition(a,b) == a + b
  }

  property("scalaSubtract") = forAll { (a: Int, b: Int) =>
    IntegerOpScala.subtract(a, b) == a - b
  }
}
